/**
 * @author Matheus Heiden
 */

import negocio.Adjacencias;
import negocio.BoundryFirstSearch;
import negocio.DepthFirstSearch;

public class Main {
	
	public static void main(String[] args) {
		/*
		 * Adjacencias adj = new Adjacencias(new int[][]{
													{0,1,1,0,1},	
													{1,0,1,1,0},
													{0,1,0,1,0},
													{1,1,1,0,1},
													{1,0,0,1,0}
													}
										); */
		Adjacencias adj = new Adjacencias(new int[][]{
													{0,1,1,1,0,1,0},	
													{0,0,0,0,1,0,0},
													{0,1,0,1,0,0,1},
													{0,0,0,0,0,0,0},
													{0,0,0,0,0,0,0},
													{0,0,0,0,0,0,0},
													{0,0,0,0,0,0,0}
													}
										);

		System.out.println(adj.isDriven());
		System.out.println(adj.isMultigraph());
		System.out.println(adj.exitDegree(2));
		System.out.println("hue");
		/**
		DepthFirstSearch df = new DepthFirstSearch(adj);
		df.setStartingVertex(2);
		df.searchGraph();
		df.printResult();
		df.printRoutingTable(); */
		
		BoundryFirstSearch bf = new BoundryFirstSearch(adj);
		bf.setStartingVertex(2);
		bf.search();
		
		bf.printRoutingTable(null);
		
	}

}

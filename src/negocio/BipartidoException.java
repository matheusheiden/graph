/**
 * @author matheus heiden
 */
package negocio;

public class BipartidoException extends Exception {
	
	public BipartidoException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}

package negocio;

public class Main {
	public static void main(String[] args) {
		Adjacencias adj = new Adjacencias(new int[][]{
			{0,1,0,0,0},	
			{1,0,1,0,0},
			{0,1,0,1,0},
			{0,0,1,0,1},
			{0,0,0,1,0}
			});
		
		Goodman go = new Goodman(adj);
		go.generateConn();
		System.out.println("conexo: "+go.isConexo());
	}
}

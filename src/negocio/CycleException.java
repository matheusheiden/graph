/**
 * @author matheus heiden
 */
package negocio;

public class CycleException extends Exception {

	public CycleException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}

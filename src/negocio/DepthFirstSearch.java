/**
 * @author matheus heiden
 */
package negocio;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class DepthFirstSearch {
	
	private Adjacencias adj;
	
	private ArrayList<Node> nodes;
	
	int tempoTotal = 0;
	
	int startingVertex = 0;
	
	boolean cycleSearch= false;
	
	public DepthFirstSearch(Adjacencias adj) {
		this.adj = adj;
		nodes = new ArrayList<>();
		nodes.ensureCapacity(adj.getVertexQty());
	}
	
	public DepthFirstSearch(Adjacencias adj, boolean cycle) {
		this.adj = adj;
		this.cycleSearch = cycle;
		nodes = new ArrayList<>();
		nodes.ensureCapacity(adj.getVertexQty());
	}
	
	public void setStartingVertex(int v) {
		startingVertex = v;
	}
	
	public void searchGraph() throws CycleException {
		//adding starting node
		Node temp = new Node();
		temp.setId(startingVertex);
		temp.setNodeState(State.NOT_EXPLORED);
		temp.setNodeColor(Cores.AZUL);
		nodes.add(temp);
		for (int i = 0; i < adj.getVertexQty(); i++) {
			
			if (i != startingVertex){
				Node temp2 = new Node();
				temp2.setId(i);
				temp2.setNodeState(State.NOT_EXPLORED);
				nodes.add(temp2);
			}
			
			
		}
		
		for (Node n : nodes) {
			if (n.getNodeState() == State.NOT_EXPLORED) {
				this.visit(n);
			}
		}
	}
	
	public void visit(Node n) throws CycleException {
		n.setNodeState(State.EXPLORED);
		this.tempoTotal = tempoTotal + 1;
		n.setTempoIni(this.tempoTotal);
		setNodeById(n);
		int[] adjIds = adj.getAdjVertex(n.getId());
		for (Node node : nodes) {
			if (IntStream.of(adjIds).anyMatch(x -> x == node.getId())) {
				if (node.getNodeState() == State.NOT_EXPLORED) {
					node.setNodeColor(this.getColorToDefine(n));
					n.addChild(node);
					this.visit(node);
				}
				else if (node.getNodeState() == State.EXPLORED && node.getId() != n.getId()) {
					n.addChild(node);
					setNodeById(n);
					if (this.cycleSearch) {
						throw new CycleException("Found a cycle");
					}
				}
				else if(node.getId() != n.getId()) {
					n.addChild(node);
					setNodeById(n);
				}
				
			}
		}
		tempoTotal++;
		n.setNodeState(State.FULLY_EXPLORED);
		setNodeById(n);
		n.setTempoFim(tempoTotal);
		
		
	}
	
	private Node getNodeById(int id) {
		for (Node n : nodes) {
			if (n.getId() == id) {
				return n;
			}
		}
		return null;
	}
	
	private void setNodeById(Node n) {
		for (int i = 0; i < nodes.size(); i++) {
			Node no = nodes.get(i);
			if (no.getId() == n.getId()) {
				nodes.set(i, n);
			}
			
		}
	}
	
	public void printResult(){
		for (Node n : nodes) {
			System.out.println("NODE V"+n.getId()+" - "+n.getTempoIni()+ "/"+n.getTempoFim());
		}
	}
	
	public void printRoutingTable() {
		System.out.print("x | ");
		for (Node n : nodes) {
			System.out.print("V"+n.getId()+" | ");
		}
		System.out.println();
		System.out.print("P | ");
		for (Node n : nodes){
			Node temp = getParentOfNode(n);
			if(temp == null) {
				System.out.print("NN | ");
			}
			else {
				System.out.print("V"+temp.getId()+" | ");
			}
			
		}
	}
	
	private Node getParentOfNode(Node n) {
		for (Node e : nodes) {
			if (e.isNodeChild(n)) {
				return e;
			}
		}
		return null;
	}
	
	private Cores getColorToDefine(Node n){
		if (n.getNodeColor() == null) {
			return Cores.AZUL;
		}
		else if  (n.getNodeColor() == Cores.AZUL) {
			return Cores.VERMELHO;
		}
		else {
			return Cores.AZUL;
		}
	}
	
	public boolean isBiparted() {
		for (Node n : nodes) {
			int[] adjIds = adj.getAdjVertex(n.getId()); 
			for (int id : adjIds) {
				Node temp = this.getNodeById(id);
				if (temp.getNodeColor() == n.getNodeColor()){
					return false;
				}
			}
		}
		return true;
	}
}

/**
 * @author matheus heiden
 */
package negocio;

public enum State {
	NOT_EXPLORED,
	EXPLORED,
	FULLY_EXPLORED
}

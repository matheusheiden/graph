/**
 * @author Matheus Heiden
 */
package negocio;

import java.util.ArrayList;

public class Adjacencias {

	int [][] listaDeAdjacencias;
	
	Boolean isMultigraph;

	public Adjacencias(int [][] listaDeAdjacencias) {
		this.listaDeAdjacencias = listaDeAdjacencias;
		
	}
	
	public int getVertexDegree(int vertex) {
		int count = 0;
		for (int i = 0; i < listaDeAdjacencias[vertex].length; i++) {
			count = count +listaDeAdjacencias[vertex][i];
		}
		
		return count;
	}
	
	public boolean isRegular() {
		for (int i = 1; i < listaDeAdjacencias.length; i++) {
			if (this.getVertexDegree(i-1) != this.getVertexDegree(i) ) {
				return false;
			}
		}
		return true;
	}
	
	public boolean isComplete() {
		for (int i = 0; i < listaDeAdjacencias.length; i++) {
			for (int j = 0; j < listaDeAdjacencias[i].length; j++) { 
				if (i != j && listaDeAdjacencias[i][j] == 0) {
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean isNull() {
		for (int i = 0; i < listaDeAdjacencias.length; i++) {
			for (int j = 0; j < listaDeAdjacencias[i].length; j++) { 
				if (i != j && listaDeAdjacencias[i][j] != 0) {
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean isBiparted() {
		return !(isComplete() && getVertexQty() > 3);
	}
	
	public int getEdgeQty() {
		int count = 0;
		for (int i = 0; i < listaDeAdjacencias.length; i++) {
			count = count + this.getVertexDegree(i);
		}
		return count/2;
	}
	
	public boolean hasLoop() {
		for (int i = 0; i < listaDeAdjacencias.length; i++) {
			for (int j = 0; j < listaDeAdjacencias[i].length; j++) {
				if (i == j && listaDeAdjacencias[i][j] > 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean hasParalel () {
		for (int i = 0; i < listaDeAdjacencias.length; i++) {
			for (int j = 0; j < listaDeAdjacencias[i].length; j++) {
				if (i != j && listaDeAdjacencias[i][j] > 1) {
					return true;
				}
			}
		}
		return false;
	}
	
	public Boolean isMultigraph() {
		if (this.isMultigraph == null)
			this.isMultigraph = hasParalel() || hasLoop();
		return this.isMultigraph;
	}
	
	public boolean isDriven() {
		for (int i = 0; i < listaDeAdjacencias.length; i++) {
			for (int j = i; j < listaDeAdjacencias[i].length; j++) {
				if (listaDeAdjacencias[i][j] != listaDeAdjacencias[j][i]) {
					return true;
				}
			}
		}
		return false;
	}
	
	public int exitDegree(int vertex) {
		int count = 0;
		for (int i = 0; i < listaDeAdjacencias[vertex].length; i++) {
			count = count + listaDeAdjacencias[vertex][i];
		}
		return count;
	}
	
	public int entranceDegree(int vertex) {
		int count = 0;
		for (int i = 0; i < listaDeAdjacencias.length; i++) {
			count = count + listaDeAdjacencias[i][vertex];
		}
		return count;
	}
	
	public int getVertexQty() {
		return listaDeAdjacencias.length;
	}
	
	public int[] getAdjVertex(int vertex) {
		ArrayList<Integer> ret = new ArrayList<Integer>();
		
		
		for (int i = 0; i < this.listaDeAdjacencias[vertex].length; i++) {
			if (this.listaDeAdjacencias[vertex][i] > 0) {
				for (int j = 0; j < this.listaDeAdjacencias[vertex][i]; j++) {
					ret.add(i);
				}
			}
		}
		int[] retArr = new int[ret.size()];
		int count = 0;
		for (Integer i : ret) {
			retArr[count] = i;
			count++;
		}
		
		return retArr;
	}
	
	public String getEdgeGroups() {
		String ret = "E = {";
		for (int i = 0; i < listaDeAdjacencias.length; i++) {
			for (int j = 0; j < listaDeAdjacencias[i].length; j++) { 
				if (this.listaDeAdjacencias[i][j] > 0) {
					for (int x = 0; x < this.listaDeAdjacencias[i][j]; x++) {
						ret = ret + "(V"+i+", V"+j+")";
					}
					
					if (i != listaDeAdjacencias.length -1 ) {
						ret = ret + ", ";
					}
					
				}
			}
		}
		ret = ret + " }";
		return ret;
	}
	
	public void print() {
		for (int i = 0; i < listaDeAdjacencias.length; i++) {
			System.out.print("V"+(i+1)+" | ");
		}
		System.out.println();
		for (int i = 0; i < listaDeAdjacencias.length; i++) {
			for (int j = 0; j < listaDeAdjacencias[i].length; j++) { 
				System.out.print(listaDeAdjacencias[i][j]+" | ");
			}
			System.out.println();
		}
	}
	
	
	
	

}

/**
 * @author matheus heiden
 */
package negocio;

import java.util.HashMap;
import java.util.Map.Entry;

public class Helper {
	public static String getKeyByValue(HashMap<String, Integer> map, int value) {
		for (Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
		return null;
	}
}

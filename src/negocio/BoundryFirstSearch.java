/**
 * @author matheus heiden
 */
package negocio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.IntStream;

public class BoundryFirstSearch {
	
	private Adjacencias adj;
	
	private ArrayList<Node> nodes;
	
	int tempoTotal = 0;
	
	int startingVertex = 0;
	
	private Queue<Node> queue;
	
	public BoundryFirstSearch(Adjacencias adj) {
		this.adj = adj;
		this.queue = new LinkedBlockingQueue<>();
		nodes = new ArrayList<>();
		nodes.ensureCapacity(adj.getVertexQty());
	}
	
	public void setStartingVertex(int v) {
		startingVertex = v;
	}
	
	public void search() {
		for (int i = 0; i < adj.getVertexQty(); i++) {	
			Node temp2 = new Node();
			temp2.setId(i);
			temp2.setNodeState(State.NOT_EXPLORED);
			nodes.add(temp2);
		}
		Node initialNode = this.getNodeById(this.startingVertex);
		initialNode.setNodeState(State.EXPLORED);
		queue.add(initialNode);
		while (!queue.isEmpty()) {
			Node u = queue.remove();
			int[] adjIds = adj.getAdjVertex(u.getId());
			for (Node node : nodes) {
				if (IntStream.of(adjIds).anyMatch(x -> x == node.getId())) { 
					if (node.getNodeState() == State.NOT_EXPLORED) {
						node.setNodeState(State.EXPLORED);
						node.setParent(u);
						node.setDistance(u.getDistance()+1);
						this.setNodeById(node);
						queue.add(node);
					}
				}
			}
			u.setNodeState(State.FULLY_EXPLORED);
			this.setNodeById(u);
		}
	}
	
	public Node getNodeById(int id) {
		for (Node n : nodes) {
			if (n.getId() == id) {
				return n;
			}
		}
		return null;
	}
	
	public void setNodeById(Node n) {
		for (int i = 0; i < nodes.size(); i++) {
			Node no = nodes.get(i);
			if (no.getId() == n.getId()) {
				nodes.set(i, n);
			}
			
		}
	}
	
	public void printRoutingTable(HashMap<String, Integer> relation) {
		System.out.print("x | ");
		
		if (relation != null) {
			for (Node n : nodes) {
				System.out.print(Helper.getKeyByValue(relation, n.getId()).toUpperCase()+" | ");
			}
		}else  {
			for (Node n : nodes) {
				System.out.print("V"+n.getId()+" | ");
			}
		}
		
		System.out.println();
		System.out.print("P | ");
		for (Node n : nodes){
			Node temp = getParentOfNode(n);
			if(temp == null) {
				System.out.print("NN | ");
			}
			else {
				if (relation != null) {
					System.out.print(Helper.getKeyByValue(relation, temp.getId()).toUpperCase()+" | ");
				}
				else {
					System.out.print("V"+temp.getId()+" | ");
				}
			}
			
		}
		System.out.println();
		System.out.print("D | ");
		for (Node n : nodes){
			System.out.print(" "+n.getDistance()+" | ");
		}
	}
	private Node getParentOfNode(Node n) {
		return n.getParent();
	}
}

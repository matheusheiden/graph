/**
 * @author matheus heiden
 */
package negocio;

import java.util.ArrayList;

public class Node {
	
	private int id;
	
	private State nodeState;
	
	private Cores nodeColor;
	
	private ArrayList<Node> children;
	
	private ArrayList<Node> components;

	private int tempoIni = 0;
	
	private int tempoFim = 0;
	
	private Node parent;
	
	private double distance = 0;
	
	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public Node() {
		children = new ArrayList<>();
		components = new ArrayList<>();
	}
	
	public int getTempoIni() {
		return tempoIni;
	}

	public void setTempoIni(int tempoIni) {
		this.tempoIni = tempoIni;
	}

	public int getTempoFim() {
		return tempoFim;
	}

	public void setTempoFim(int tempoFim) {
		this.tempoFim = tempoFim;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public State getNodeState() {
		return nodeState;
	}

	public void setNodeState(State nodeState) {
		this.nodeState = nodeState;
	}
	
	public Cores getNodeColor() {
		return nodeColor;
	}

	public void setNodeColor(Cores nodeColor) {
		this.nodeColor = nodeColor;
	}

	public Node getChildren(int pos) {
		return children.get(pos);
	}
	
	public ArrayList<Node> getAllChildren() {
		return children;
	}

	public void addChild(Node child) {
		this.children.add(child);
	}
	
	public boolean isNodeChild(Node n) {
		for (Node no : this.children) {
			if (no.getId() == n.getId()) {
				return true;
			}
		}
		return false;
	}
	
	public void addComponent(Node n) {
		boolean shouldAdd = true;
		for (Node no : components) {
			if (no.getId() == n.getId()) {
				shouldAdd = false;
			}
		}
		if (shouldAdd)
			components.add(n);
	}
	
	public boolean hasComponent (Node n) {
		for (Node no : components) {
			if (n.getId() == no.getId()) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasComponentById (int n) {
		for (Node no : components) {
			if (n == no.getId()) {
				return true;
			}
		}
		return false;
	}
	
	public Node getComponentById (int n) {
		for (Node no : components) {
			if (n == no.getId()) {
				return no;
			}
		}
		return null;
	}

}

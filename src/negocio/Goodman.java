package negocio;

import java.util.ArrayList;
import java.util.Iterator;

public class Goodman {
	
	private ArrayList<Node> grafo;
	
	private ArrayList<Node> grafoTemp;
	
	private int countConn = 0;
	
	private Adjacencias adjMatrix; 
	
	public Goodman (Adjacencias matrixAdj) {
		this.adjMatrix = matrixAdj;
		grafo = new ArrayList<>();
		grafoTemp = new ArrayList<>();
		initialize();
	}
	
	public void initialize() {
		for (int i = 0; i < adjMatrix.getVertexQty(); i++) {
			Node n = new Node();
			n.setId(i);
			grafo.add(n);
		}
	}
	
	public void generateConn() {
		while (!grafo.isEmpty()) {
			Node n = grafo.get(0);
			while (getAdjNodes(n) != null && getComponent(n) == null) {
				Node adjNode = getAdjNodes(n);
				if (getComponent(adjNode) != null) {
					Node w = getComponent(adjNode);
					w.addComponent(adjNode);
					w.addComponent(n);
					updateConn(w);
				}
				else {
					Node w = new Node();
					w.setId(countConn);
					w.addComponent(n);
					w.addComponent(adjNode);
					grafoTemp.add(w);
				}
				removeNode(adjNode.getId());
				
			}
			removeNode(n.getId());
			
		}
	}
	
	public Node removeNode(int id) {
		for (Iterator<Node> it = grafo.iterator(); it.hasNext();) {
			Node n = it.next();
			if (n.getId() == id) {
				it.remove();
				return n;
			}
		}
		return null;
	}
	
	public Node getAdjNodes(Node n) {
		int[] adjs = adjMatrix.getAdjVertex(n.getId());
		if (adjs.length == 0) {
			return n;
		}
		for (int id : adjs) {
			for (Node no : grafo) {
				if (no.getId() == id) {
					return no;
				}
			}
			for (Node no : grafoTemp) {
				if (no.hasComponentById(id)) {
					return no.getComponentById(id);
				}
			}
		}
		
		return null;
	}
	
	public boolean isConexo() {
		return grafoTemp.size() == 1;
	}
	
	public Node getComponent(Node n) {
		for (Node no : grafoTemp) {
			if (no.hasComponent(n)) {
				return no;
			}
		}
		return null;
	}
	
	public void updateConn(Node n) {
		for (int i = 0; i < grafoTemp.size(); i++) {
			if (grafoTemp.get(i).getId() == n.getId()) {
				grafoTemp.set(i, n);
			}
		}
	}
	
	
	
}

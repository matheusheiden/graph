package negocio;

import java.util.ArrayList;
import java.util.Collections;

import trabalho.segundo.Node;

public class Dijkstra {
	
	private ArrayList<Node> nodes;
	
	private Adjacencias adj;
	
	private ArrayList<Node> Q;
	
	private ArrayList<Node> S;
	
	private Node destiny;
	
	
	public Dijkstra(ArrayList<Node> nodes, Adjacencias adj, Node startingPoint, Node destiny) {
		this.nodes = nodes;
		this.adj = adj;
		
		this.Q = nodes;
		this.initializeSingleSource(startingPoint);
		this.S = new ArrayList<>();
		this.destiny = destiny;
		
		while (!Q.isEmpty()) {
			Node n = this.extractMin();
			S.add(n);
			int[] nAdj = this.adj.getAdjVertex(n.getId());
			for (int adjV : nAdj) {
				Node ad = this.getNodeById(adjV);
				if (ad != null)
					this.relax(n, ad);
			}
		}
	}
	
	
	private void initializeSingleSource(Node sp) {
		for (Node n : nodes) {
			n.setDistance(Integer.MAX_VALUE);
			n.setParent(null);
			this.setNodeById(n);
		}
		
		sp.setDistance(0);
		this.setNodeById(sp);
		
	}
	
	private void relax(Node orig, Node destino) {
		if (destino.getDistance() > (orig.getDistance() + trabalho.segundo.Main.distanceBetweenNodes(orig, destino)) ) {
			destino.setParent(orig);
			destino.setDistance(orig.getDistance() + trabalho.segundo.Main.distanceBetweenNodes(orig, destino));
			this.setNodeById(destino);
		}
	}
	
	private Node extractMin() {
		Node n = Collections.min(this.Q);
		removeNode(n);
		return n;
		
	}
	
	public Node getNodeById(int id) {
		for (Node n : Q) {
			if (n.getId() == id) {
				return n;
			}
		}
		return null;
	}
	
	public void setNodeById(Node n) {
		for (int i = 0; i < Q.size(); i++) {
			Node no = Q.get(i);
			if (no.getId() == n.getId()) {
				Q.set(i, n);
			}
		}
	}
	
	public void removeNode(Node n) {
		for (int i = 0; i < Q.size(); i++) {
			Node no = Q.get(i);
			if (no.getId() == n.getId()) {
				Q.remove(i);
				break;
			}
		}
	}
	
	public void printResult() {
		ArrayList<String> text = new ArrayList<>();
		System.out.println("  X  |  Y  |  Vértice   |  Distância  |");
		Node current = this.getFinishedNodeById(this.destiny.getId());
		text.add("\t\tTotal   | "+current.getDistance());
		while (current !=null) {
			text.add("  "+current.getX()+"  |  "+current.getY()+"  |\tV"+(current.getId()+1)+"\t| "+trabalho.segundo.Main.distanceBetweenNodes(current.getParent(),current));
			current = current.getParent();
		}
		
		Collections.reverse(text);
		
		for (String s : text) {
			System.out.println(s);
		}
			
	}
	
	public Node getFinishedNodeById(int id) {
		for (Node n : S) {
			if (n.getId() == id) {
				return n;
			}
		}
		return null;
	}
	
	
	
	
	
	

}

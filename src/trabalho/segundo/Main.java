package trabalho.segundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	public static void main(String[] args) {
		try {
			System.out.println("Qual o nome do arquivo de entrada?");
			
			String filename = new Scanner(System.in).next();
			
			String input = loadInput(filename);
			
			String[] temp = input.split("\n");
			
			input = input.substring(input.indexOf('\n')+1);
			
			int[] vertx = new int[2];
			int count = 0;
			Pattern pt = Pattern.compile("([0-9]+)");
			Matcher ma = pt.matcher(temp[0]);
			while (ma.find()) {
				String node = ma.group();
				vertx[count] = Integer.parseInt(node)-1;
				count++;
				
			}
			
			
			Trabalho trab = new Trabalho(input, vertx[0], vertx[1]);
			
			
			
			
		}
		catch (Exception e) {
			
			e.printStackTrace();
			Main.main(args);
		}
		
	}
	
	public static String loadInput(String filename) throws IOException {
		//path windows
		//String path = "C:"+File.separator+"temp"+File.separator+filename;
		//path linux
		String path = File.separator+"tmp"+File.separator+filename;
		
		try(BufferedReader br = new BufferedReader(new FileReader(path))) {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		   return sb.toString();
		}
	}
	
	public static double distanceBetweenNodes(Node n1, Node n2) {
		if (n1 == null || n2 == null) {
			return 0;
		}
		return Math.sqrt(Math.pow((n2.getX() - n1.getX()), 2) + Math.pow((n2.getY() - n1.getY()), 2));
	}

}

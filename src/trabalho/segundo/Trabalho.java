package trabalho.segundo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import negocio.Adjacencias;
import negocio.Dijkstra;

public class Trabalho {
	
	private String input;
	
	private String[] edges;
	
	private ArrayList<trabalho.segundo.Node> original;
	
	private ArrayList<trabalho.segundo.Node> nodes;
	
	private int[][] adj;
	
	private Adjacencias realAdj;
	
	private ArrayList<Node> trash;
	
	
	public Trabalho (String input, int startingPoint, int finish) {
		this.input = input;
		nodes = new ArrayList<>();
		trash = new ArrayList<>();
		
		parseInput();
		
		executeDijkstra(startingPoint, finish);
	}
	
	private void executeDijkstra(int startingPoint, int finish) {
		realAdj.print();
		Dijkstra dij = new Dijkstra(nodes, realAdj, this.getNodeById(startingPoint), this.getNodeById(finish));
		dij.printResult();
	}
	
	public void parseInput() {
		edges = input.split("\n");
		for (int i = 0; i < edges.length; i++) {
			edges[i] = edges[i].trim();
		}
		Pattern pt = Pattern.compile("([0-9]+\\,)\\w+");
		for (String st : edges) {
			Matcher ma = pt.matcher(st);
			while (ma.find()) {
				String node = ma.group();
				String[] xAndY = node.split(",");
				addNode(Integer.parseInt(xAndY[0]),Integer.parseInt(xAndY[1]));
			}
		}
		this.original = nodes;
		/**
		 * prepare adj matrix
		 */
		adj = new int[nodes.size()][nodes.size()];
		for (String st : edges) {
			Matcher ma = pt.matcher(st);
			ArrayList<Node> conn = new ArrayList<>(); 
			while (ma.find()) {
				String node = ma.group();
				String[] xAndY = node.split(",");
				conn.add(getNodeByXandY(Integer.parseInt(xAndY[0]),Integer.parseInt(xAndY[1])));
			}
			adj[conn.get(0).getId()][conn.get(1).getId()]++;
			adj[conn.get(1).getId()][conn.get(0).getId()]++;
		}
		realAdj = new Adjacencias(adj);
		
		realAdj.print();
		
		int radius = 1;
		int count = 0;
		while (findLowDegreeVertex().size() > 0 ) {
			unifyLowDegree(radius);
			if (count % 100 == 0) {
				System.out.println("Aumentado raio da busca");
				radius++;
			}
			count++;
			//break;
		}
		
	}
	
	public void addNode(int x, int y) {
		Node n = new Node();
		n.setX(x);
		n.setY(y);
		n.setId(nodes.size());
		if (!hasNode(n)) {
			nodes.add(n);
		}
	}
	
	private boolean hasNode(Node no) {
		for(Node n : nodes) {
			if (no.getX() == n.getX() && no.getY() == n.getY()) {
				return true;
			}
		}
		
		return false;
	}
	
	private Node getNodeByXandY(int x, int y) {
		for(Node n : nodes) {
			if (x == n.getX() && y == n.getY()) {
				return n;
			}
		}
		return null;
	}
	
	private Node removeNodeById(int id) {
		Node o = this.getNodeById(id);
		nodes.remove(o);
		return o;
	}
	
	private Node getNodeById(int id) {
		for (Node n : nodes) {
			if (n.getId() == id) {
				return n;
			}
		}
		return null;
	}
	
	private void decreaseIdsStartingAt (int startingPoint) {
		/**printNode();
		for (int i = startingPoint; i < nodes.size(); i++ ) {
			Node n = nodes.get(i) ;
			n.setId(n.getId() - 1);
			nodes.set(n.getId(), n);
		}
		System.out.println("Hue");
		printNode();
		
		System.exit(0);
*/
		ArrayList<Node> newNodes = new ArrayList<>();
		for (int i = 0; i < realAdj.getVertexQty()-1; i ++) {
			Node n = new Node();
			if ( i >= startingPoint) {
				n.setId(i);
				n.setX(this.getNodeById(i+1).getX());
				n.setY(this.getNodeById(i+1).getY());
			}
			else {
				n.setId(i);
				n.setX(this.getNodeById(i).getX());
				n.setY(this.getNodeById(i).getY());
			}
			
			newNodes.add(n);
		}
		this.nodes = newNodes;
		
	/**	this.removeNodeById(startingPoint);
		for (int i = startingPoint+1; i < nodes.size(); i++) {
			if (i == startingPoint) {
				continue;
			}
			Node n = this.getNodeById(i);
			if (n == null) {
				System.out.println(i);
				continue;
			}
			System.out.print("X "+ n.getX()+"  Y "+n.getY()+"  ID: "+ n.getId());
			n.setId(n.getId()-1);
			System.out.println(" AFTER: "+n.getId());
			nodes.set(i,n);
		}
		System.out.println("hue");*/
	}
	
	public void setNodeById(Node n) {
		for (int i = 0; i < nodes.size(); i++) {
			Node no = nodes.get(i);
			if (no.getId() == n.getId()) {
				nodes.set(i, n);
			}
			
		}
	}
	
	public void printNode() {
		for (Node n : nodes) {
			System.out.println("V"+n.getId()+" "+n.getX()+" "+n.getY());
		}
	}
	
	public ArrayList<Node> findLowDegreeVertex() {
		
		ArrayList<Node> lowDegree = new ArrayList<>();
		for (Node n : nodes) {
			if (realAdj.getVertexDegree(n.getId()) == 1 && !trash.contains(n)) {
				lowDegree.add(n);
			}
		}
		return lowDegree;
	}
	
	public void unifyLowDegree(int limit) {
		/**ArrayList<Node> lowDegree = findLowDegreeVertex();
		for (Node n : lowDegree) {
			for (Node no : nodes) {
				if (n.getId() != no.getId()) {
					if ( Main.distanceBetweenNodes(n, no) <= limit  ) {
						System.out.println("Unifying "+n.getId() + " And "+ no.getId());
						unifyNodes(n, no);
						break;
					}
				}
			}
			break;
		}**/
		ArrayList<Node> lowDegree = findLowDegreeVertex();
		ArrayList<Node> temp = new ArrayList<>();
		
		Node firstLow = lowDegree.get(0);
		
		for (Node n : nodes) {
			if ( firstLow.getId() != n.getId() &&  Main.distanceBetweenNodes(firstLow, n) <= limit) {
				n.setDistance( Main.distanceBetweenNodes(firstLow, n) );
				temp.add(n);
			}
		}
		if (temp.size() > 0 ) {
			System.out.println("Unifying "+firstLow.getId() + " And "+ Collections.min(temp).getId());
			unifyNodes(firstLow, Collections.min(temp));
		}
		
		
		
	}
	
	public void unifyNodes(Node n, Node no) {
		printNode();
		int[] adjN = adj[n.getId()];
		int[] adjNo = adj[no.getId()];
		for (int i = 0; i < adjN.length; i++) {
			adjN[i] = adjN[i] + adjNo[i];
		}
		int idToRemove = n.getId();
		int idToKeep = no.getId();
		if (no.getId() > idToRemove) { //remove the highest node
			idToRemove = no.getId();
			idToKeep = n.getId();
		}
		
		int [][] newAdj = new int[adj.length-1][adj.length-1];
		
		int idLinha = 0;
		int idColuna = 0;
		linha : for (int i = 0; i < adj.length; i++) {
			idColuna = 0;
			if (i == idToRemove ) { //removes the line
				idLinha = i;
				continue linha;
			}
			coluna : for (int j = 0; j < adj[i].length; j++) {
				if (j == idToRemove) { //removes the column
					idColuna = j;
					continue coluna;
				}
				
				newAdj[idLinha][idColuna] = adj[i][j];
				idColuna++;
			}
			idLinha++;
		}
		int temp = 0;
		/**
		for (int i = 0; i < adjN.length; i++) { //update columns
			if (i == idToRemove) {
				temp--;
				continue;
			}
			newAdj[idToKeep][temp] = adjN[i];
			temp++;
		}
		
		temp = 0;
		
		for (int i = 0; i < adjN.length; i++) { //update lines
			if (i == idToRemove) {
				temp--;
				continue;
			}
			newAdj[temp][idToKeep] = adjN[i];
			temp++;
		} **/
		//this.removeNodeById(idToRemove);//remove node from nodes array list
		this.trash.add(this.getNodeById(idToRemove));
		
		this.realAdj = new Adjacencias(newAdj);
		
		this.adj = newAdj;
		
		this.decreaseIdsStartingAt(idToRemove);
		
		
		
		
		realAdj.print();
		
		ArrayList<Node> nn = this.findLowDegreeVertex();
		
	}
	
}

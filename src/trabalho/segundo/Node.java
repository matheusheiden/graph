package trabalho.segundo;

public class Node extends negocio.Node implements Comparable<Object> {
	
	private int idOrig;
	
	private int x;
	
	private int y;
	
	private trabalho.segundo.Node parent;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public int compareTo(Object o) {
		if (!(o instanceof Node))
			throw new ClassCastException("A Node object expected.");
		double another = ((Node) o).getDistance();  
		return (int)(this.getDistance() - another);
	}
	
	public trabalho.segundo.Node getParent() {
		return parent;
	}

	public void setParent(trabalho.segundo.Node parent) {
		this.parent = parent;
	}
	
	
}

/**
 * @author matheus heiden
 */
package trabalho.primeiro;

import java.util.ArrayList;
import java.util.HashMap;

import negocio.Adjacencias;
import negocio.CycleException;
import negocio.DepthFirstSearch;

public class Dudu {
	
	
	private ArrayList<HashMap<String, Integer>> vertexNameRelations;
	
	private Reader defReader;
	
	private ArrayList<Reader> caseReaders;
	
	private int testCaseAmount = 0;
	
	private int[] qtyVertexByCase;
	
	private int[] qtyEdgeByCase;
	
	private ArrayList<int[][]> adjs; 
	
	//private int[][] adj;
	
	public Dudu() {
		vertexNameRelations = new ArrayList<>();
		defReader = new Reader();
		caseReaders = new ArrayList<>();
		adjs = new ArrayList<>();
	}
	
	
	private void prepareTestCaseQuantity() {
		System.out.println("Insira a quantidade de casos de teste.");
		try  {
			defReader.readInput(new Validation() {

				@Override
				public void validate(String content) throws Exception {
					
					int qty = Integer.parseInt(content);
					
					if (qty > 100) {
						throw new Exception("Quantidade maxima de casos de testes é 100");
					}
					
				}
				
			});
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		testCaseAmount = Integer.parseInt(defReader.getLine(0));
		
		for (int i = 0; i < testCaseAmount; i++) {
			caseReaders.add(new Reader());
		}
		qtyVertexByCase = new int[testCaseAmount];
		
		qtyEdgeByCase = new int[testCaseAmount];
		

	}
	
	private void prepareCases()  {
		
		for (int i = 0; i < testCaseAmount; i++) {
			
			System.out.println("Preparando Quantidade de vertices e arestas");
			
			Reader temp = caseReaders.get(i);
			try  {
				temp.readInput(new Validation() {

					@Override
					public void validate(String content) throws Exception {
						
						String[] values = content.split(" ");

						if (values.length > 2) {
							throw new Exception("Apenas 2 valores sao permitidos aqui");
						}
						
						int docs = Integer.parseInt(values[0]);
						
						if (docs < 2 || docs > 100 ) {
						throw new Exception("Quantidade de documentos invalida");
						}
						
						int dep = Integer.parseInt(values[1]);
						
						if (dep < 1 || dep  > 300 ) {
						throw new Exception("Quantidade de dependencias invalida");
						}
						
					}
					
				});
				
				String[] firstInformation = temp.getLine(0).split(" ");
				
				qtyVertexByCase[i] = Integer.parseInt(firstInformation[0]);
				
				qtyEdgeByCase[i] = Integer.parseInt(firstInformation[1]);
			}
			catch (Exception e) {
				System.out.println(e.getMessage());
				i--;
			}
			
			System.out.println("Pegando arestas");
			
			for (int j = 0; j < this.qtyEdgeByCase[i]; j++  ) {
				try {
					temp.readInput(new Validation() {
						
						@Override
						public void validate(String content) throws Exception {
							
							String[] values = content.split(" ");
	
							if (values.length > 2 || values.length < 1) {
								throw new Exception("Apenas 2 valores sao permitidos aqui");
							}
							
						}
					});
				} catch (Exception e) {
					System.out.println(e.getMessage());
					i--;
				}
			}
		
		
		//temp.finish();
		caseReaders.set(i, temp);
		}
	}
	
	private void prepareAdjs() {
		for (int i = 0; i < testCaseAmount; i++) {
			Reader temp = caseReaders.get(i);
			try {
				int vertexCounter = 0;
				vertexNameRelations.add(new HashMap<String, Integer>());
				for (int j = 1; j <= qtyEdgeByCase[i]; j++) { // starting at one because first line specifies the quantity of vertexes and edges
					String strtemp = temp.getLine(j);
					String[] vertexes = strtemp.split(" ");
					
					for (String v : vertexes) {
						
						if (!vertexNameRelations.get(i).containsKey(v.toLowerCase())) {
							vertexNameRelations.get(i).put(v.toLowerCase(), vertexCounter);
							vertexCounter++;
						}
						
					}
				}
				int[][] adj = new int[qtyVertexByCase[i]][qtyVertexByCase[i]];
				for (int j = 1; j <= qtyEdgeByCase[i]; j++) { // starting at one because first line specifies the quantity of vertexes and edges
					String strtemp = temp.getLine(j);
					String[] vertexes = strtemp.split(" ");
					adj[vertexNameRelations.get(i).get(vertexes[0].toLowerCase())][vertexNameRelations.get(i).get(vertexes[1].toLowerCase())] ++;
				//	adj[vertexNameRelations.get(i).get(vertexes[1].toLowerCase())][vertexNameRelations.get(i).get(vertexes[0].toLowerCase())] ++;
				}
				
				adjs.add(adj);
			}
			catch (Exception e) {
				System.out.println("Erro preparando: "+e.getMessage());
				e.printStackTrace();
			}
		}
	}
	public void run() {
		this.prepareTestCaseQuantity();
		this.prepareCases();
		this.prepareAdjs();
		for (int[][] adj : adjs) {
			
			try {
				DepthFirstSearch df = new DepthFirstSearch(new Adjacencias(adj), true);
				df.searchGraph();
				System.out.println("NÃO");
			}
			catch (CycleException e) {
				System.out.println("SIM");
			}
		}
	}
	
	

}

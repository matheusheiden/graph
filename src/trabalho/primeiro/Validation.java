/**
 * @author Matheus Heiden
 */
package trabalho.primeiro;

public interface Validation {
	void validate(String content) throws Exception;
}

/**
 * @author Matheus Heiden
 */
package trabalho.primeiro;

import negocio.Adjacencias;
import negocio.CycleException;
import negocio.DepthFirstSearch;

public class Main {
	public static void main(String[] args) {
		System.out.println(tipoDoGrafo(new int[][]{
			{0,1,1,0,1},	
			{1,0,1,1,0},
			{1,1,0,1,0},
			{0,1,1,0,1},
			{1,0,0,1,0}
			}));
		
		System.out.println(tipoDoGrafo(new int[][]{
			{0,1,0,0,1},	
			{1,0,1,0,0},
			{0,1,0,1,0},
			{0,0,1,0,0},
			{1,0,0,0,0}
			}));
		
		System.out.println(tipoDoGrafo(new int[][]{
			{0,0,0,1},	
			{0,0,0,1},
			{0,0,0,1},
			{1,1,1,0}
			}));
		
		//roda rato
		Rato rat = new Rato();
		rat.run();
		//roda dudu
		Dudu dudu = new Dudu();
		dudu.run();
	}
	
	public static int grausDoVertice(int[][] matrizAdj, int vertice) {
		Adjacencias adj = new Adjacencias(matrizAdj);
		return adj.getVertexDegree(vertice);
	}
	
	public static String tipoDoGrafo(int[][] matrizAdj) {
		Adjacencias adj = new Adjacencias(matrizAdj);
		String ret = "";
		if (adj.isDriven()) {
			ret = ret + " Dirigido";
		}
		if (adj.isRegular()){
			ret = ret + " Regular";
		}
		if (adj.isComplete()) {
			ret = ret + " Completo";
		}
		if (isBiparted(matrizAdj)) {
			ret = ret + " Bipartido";
		}
		else if (adj.isNull()) {
			ret = ret + " Nulo";
		}
		if (adj.isMultigraph()) {
			ret = ret +  " Multigrafo";
		}
		else {
			ret = ret + " Simples";
		}
		
		return ret.trim();
		
	}
	
	public static String arestasDoGrafo(int[][] matrizAdj) {
		Adjacencias adj = new Adjacencias(matrizAdj);
		return adj.getEdgeGroups();
	}
	
	private static boolean isBiparted(int[][] matriz) {
		DepthFirstSearch df = new DepthFirstSearch(new Adjacencias(matriz));
		try {
			df.searchGraph();
		} catch (CycleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return df.isBiparted();
	}
	
}

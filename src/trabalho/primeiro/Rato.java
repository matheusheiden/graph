/**
 * @author Matheus Heiden
 */
package trabalho.primeiro;

import java.util.HashMap;

import negocio.Adjacencias;
import negocio.BoundryFirstSearch;
import negocio.Node;

public class Rato {
	
	private HashMap<String, Integer> vertexNameRelation;
	
	private Reader reader;
	
	private int vertexQty = 0;
	
	private int edgeQty = 0;
	
	private int[][] adj;
	
	private int startingPoint = 0;
	
	private int endingPoint = 0;
	
	private int objPoint = 0;
	
	public Rato() {
		vertexNameRelation = new HashMap<>();
		reader = new Reader();
	}
	
	
	private void prepareVertexAndEdges() {
		System.out.println("Insira a quantidade de pontos e ligacoes separadas por um espaco");
		try  {
			reader.readInput(new Validation() {

				@Override
				public void validate(String content) throws Exception {
					
					String[] values = content.split(" ");

					if (values.length > 2) {
						throw new Exception("Apenas 2 valores sao permitidos aqui");
					}
					
					int pontos = Integer.parseInt(values[0]);
					
					if (pontos < 4 || pontos > 4000 ) {
					throw new Exception("Quantidade de pontos invalida");
					}
					
					int ligacoes = Integer.parseInt(values[1]);
					
					if (ligacoes < 4 || ligacoes > 5000 ) {
					throw new Exception("Quantidade de ligacoes invalida");
					}
					
				}
				
			});
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			prepareVertexAndEdges();
		}
		
		String[] firstInformation = reader.getLine(0).split(" ");
		
		vertexQty = Integer.parseInt(firstInformation[0]);
		
		edgeQty = Integer.parseInt(firstInformation[1]);

	}
	
	private void prepareMatrix () {
		adj = new int[vertexQty][vertexQty];
		
		for (int i = 0; i < adj.length; i++){
			for (int j = 0; j < adj[i].length; j++){
				adj[i][j] = 0;
			}
		}
	}
	
	private void readEdges() {
		
			for (int i = 0; i < this.edgeQty; i++  ) {
				try {
					reader.readInput(new Validation() {
						
						@Override
						public void validate(String content) throws Exception {
							
							String[] values = content.split(" ");
	
							if (values.length > 2 || values.length < 1) {
								throw new Exception("Apenas 2 valores sao permitidos aqui");
							}
							
						}
					});
				} catch (Exception e) {
					System.out.println(e.getMessage());
					i--;
				}
			}
		
		
		reader.finish();
	}
	
	private void finishMatrix() {
		try {
			int vertexCounter = 0;
			for (int i = 1; i <= edgeQty; i++) { // starting at one because first line specifies the quantity of vertexes and edges
				String temp = reader.getLine(i);
				String[] vertexes = temp.split(" ");
				
				for (String v : vertexes) {
					
					if (!vertexNameRelation.containsKey(v.toLowerCase())) {
						if (v.toLowerCase().equals("entrada")) {
							startingPoint = vertexCounter;
						}
						
						if (v.toLowerCase().equals("saida")) {
							endingPoint = vertexCounter;
						}
						
						if (v.equals("*")){
							objPoint = vertexCounter;
						}
						vertexNameRelation.put(v.toLowerCase(), vertexCounter);
						vertexCounter++;
					}
					
				}
			}
			
			if (!validateRelation()) {
				throw new Exception ("Uma saida, uma entrada e um queijo é necessario");
			}
			for (int i = 1; i <= edgeQty; i++) { // starting at one because first line specifies the quantity of vertexes and edges
				String temp = reader.getLine(i);
				String[] vertexes = temp.split(" ");
				adj[vertexNameRelation.get(vertexes[0].toLowerCase())][vertexNameRelation.get(vertexes[1].toLowerCase())] ++;
				adj[vertexNameRelation.get(vertexes[1].toLowerCase())][vertexNameRelation.get(vertexes[0].toLowerCase())] ++;
			}
		}
		catch (Exception e){
			System.out.println(e.getMessage());
			reader = new Reader();
			vertexNameRelation = new HashMap<>();
			run();
		}
		
	}
	
	/**
	 * verifies if vertexes contains at least an exit and an entry
	 * @return
	 */
	private boolean validateRelation() {
		return vertexNameRelation.containsKey("saida") && vertexNameRelation.containsKey("entrada") && vertexNameRelation.containsKey("*");
	}
	
	public void run() {
		
		this.prepareVertexAndEdges();
		this.prepareMatrix();
		this.readEdges();
		
		this.finishMatrix();
		
		BoundryFirstSearch search = new BoundryFirstSearch(new Adjacencias(adj));
		search.setStartingVertex(startingPoint);
		search.search();
		
		if (this.endPointPasses(search.getNodeById(endingPoint))){
			System.out.println(search.getNodeById(endingPoint).getDistance());
		}
		else if (search.getNodeById(objPoint).getParent() != null){
			
			BoundryFirstSearch secondSearch = new BoundryFirstSearch(new Adjacencias(adj));
			secondSearch.setStartingVertex(objPoint);
			secondSearch.search();
			if (secondSearch.getNodeById(endingPoint).getParent() == null ){
				System.out.println("Impossivel de alcançar saida apartir do queijo");
			}
			else {
				System.out.println(secondSearch.getNodeById(endingPoint).getDistance() + search.getNodeById(objPoint).getDistance());
			}

		}
		
		else {
			System.out.println("Impossivel de alcançar queijo");
		}
		
		
	}
	
	private boolean endPointPasses (Node endingPoint) {
		if (endingPoint.getParent() != null){
			return endingPoint.getId() == objPoint ? true : endPointPasses(endingPoint.getParent());
		}
		return false;
	}
}

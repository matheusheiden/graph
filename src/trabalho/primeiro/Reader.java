/**
 * @author Matheus Heiden
 */
package trabalho.primeiro;

import java.util.ArrayList;
import java.util.Scanner;

public class Reader {
	
	private ArrayList<String> input;
	
	private Scanner scan;
	
	public Reader() {
		input = new ArrayList<>();
		scan = new Scanner(System.in);
	}
	
	public void readInput(Validation val) throws Exception {
        while (!scan.hasNextLine()) {
        	
        }
		String content = scan.nextLine();
        val.validate(content);
		input.add(content);
        
	}
	
	public String getLine(int line) {
		return  input.get(line);
	}
	
	public void finish() {
		scan.close();
	}
}
